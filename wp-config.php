<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'atb_local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XiO0M+JqtdY9Vsb+L+rEKsj+MlCjAb8pzaGOg4fpHUscTXBd7tvHkRWtHKE1Ql5tq0PtBiYtkDp1gtjTkzWPNg==');
define('SECURE_AUTH_KEY',  '3AITtiqY5O6al9ep+l42/Q/g8DtIIvFrXLiRZ8yCGKaAymYu2n/LmMiMAqggLiY/8flFi3+0d1m7EULkVUXWug==');
define('LOGGED_IN_KEY',    'woQIkauoCzMkQHgdnPdvWuBPFjMuAUrS5V/Ms0aNc/kXZI8cBlIHwP66bXHSYXKbab0itMlWjpOr5AcB8z0sFQ==');
define('NONCE_KEY',        '4Fih78Q2/xKtWbfQNrivY60vXZCJ3+96SdXJ5ZyD6lajZNjwLFxJ397xCNCffuUXMnOXU+Enk7/kd093tJZ+lw==');
define('AUTH_SALT',        'WG5Dlh5vXSEg+eNrQc/Tz5agno1jpaQW65rkaX5Rf3it/GmqeOdIeHxLod/JqYrujkhebGBs95Hud/Cfs+CK3A==');
define('SECURE_AUTH_SALT', 'F9j6WDSWuzCSTZxkbN4SfUDY2DV/RTr6z0/1hOHG/ntm9kZh4JZsIX1+lv31jT+gZ7dRWhHHXp+BwpWXXyEtkQ==');
define('LOGGED_IN_SALT',   'AvaRYIDtTRQMLoxy+3hWzGFCmnplEqR6o9O9bnVyinnjUQWxAAo/B4ThRcDZfAZgV88j8E2RCdCsD3GCBpJ1uQ==');
define('NONCE_SALT',       'J7IaXfPzx4OISJScqUO2Big6e6JPgFex34v7XthrfTKGcrrGIEyQss2J6l9E8XA/ACyodOwUGqNUu9F3L6T96Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
	$_SERVER['HTTPS'] = 'on';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
