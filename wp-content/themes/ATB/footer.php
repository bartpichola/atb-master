<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

</section>
<footer class="footer">
	<div class="row">
		<div class="small-12 columns">
			<?php do_action( 'foundationpress_before_footer' ); ?>
			<?php dynamic_sidebar( 'footer-widgets' ); ?>
			<?php do_action( 'foundationpress_after_footer' ); ?>
		 	<p><strong>Subscribe to ATB and never miss a post!</strong><br><!--Plus get a free course on Genuine Marketing. Just fill out your info below:--></p>
			<?php echo do_shortcode('[yikes-mailchimp form="3"]'); ?>
		</div>
		<div class="small-12 columns">
			<ul class="inline-list">
			  <li><a href="/about-bart">About Bart</a></li>
			  <li><a href="/contact-bart">Contact Bart</a></li>
			  <li><a href="/terms-of-use">Terms of use</a></li>
			  <li><a href="/privacy">Privacy</a></li>
			  <li><a href="/affiliate-disclaimer">Disclaimer</a></li>
			 <!-- <li><a href="http://facebook.com/accordingtobart"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>			  
			  <li><a href="http://twitter.com/atb_bart"><i class="fa fa-twitter" aria-hidden="true"></i></a></li> -->
			</ul>		
			<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><a style="color:white; font-size: 0.625rem;" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"> This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.	
		</div>
	</div>
</footer>

<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>

<a class="exit-off-canvas"></a>
<?php endif; ?>

	<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
	</div>
</div>
<?php endif; ?>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
