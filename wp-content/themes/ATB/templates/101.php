<?php
/*
Template Name: ATB 101 Page Template
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>



	<?php /* Start loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				
			<div class="one01 section_container_1">
				<div class="row">
					<div class="small-12 columns">
						<h1>Thanks for checking out ATB! You’re Awesome.</h1>
					</div>
					<div class="small-12 medium-7 large-7 columns">
						<p>My premise is simple: <u>Do Better By Being Better</u>. In other words: do better marketing by improving yourself first. </p>
						<p><u>Doing better by being better</u> is a philosophy based on constant and deliberate practice of self improvement. It’s a practice of decluttering your mind, improving your most valuable skills, and regaining control over your life. </p>
						<p>Because only once you <i>start</i> regaining control, you can <u>grant yourself permission</u> to do better marketing.</p>
						<p>ATB is part <i>marketing how-to</i> and part <i>self improvement</i> blog. This is where I deconstruct the knowledge I’ve gained over the last decade as a marketer. And where I <i>try</i> to articulate the new knowledge I constantly gain through practice. </p>
						<p>My goal is to help you jumpstart your life as a marketer by saving you a decade or more of work. So please read on and enjoy. </p>
					</div>
					<div class="small-12 medium-5 large-5 columns">

						

					</div>
				</div>	
			</div>

			<div class="one01 section_container_2">
				<div class="row">
					<div class="small-12 columns">
						<h2>Getting Started</h2>
					</div>
					<div class="small-12 medium-12 large-12 columns">
					<p>Since this blog is fairly new, the content is not overwhelming (yet). Things are simple, but to simplify even further I recommend checking out the following posts and sections: </p>
						
					</div>

				</div>	
			</div>

			<div class="one01 section_container_3">
			
				<div class="row">
					<div class="large-2 columns text-center">
						<i class="fa fa-envelope-o"></i>
					</div>
					<div class="large-10 columns">
					   <p>Subscribe to marketing tidbit of the week and receive "10 Things I Wish I Knew 10 Years Ago" success guide as a gift.</p>
					</div>
				</div>
				<div class="row">
						<div class="small-12 columns">
							<form>
							  <div class="row collapse">
							    <div class="small-2 large-1 columns">
							      <span class="prefix">Name</span>
							    </div>
							    <div class="small-1 large-4 columns">
							      <input type="text" placeholder="ex. John">
							    </div>
							    <div class="small-2 large-1 columns">
							      <span class="prefix">Email</span>
							    </div>
							    <div class="small-1 large-4 columns">
							      <input type="text" placeholder="John@awesome.com">
							    </div>
							    <div class="small-2 columns">
							          <a href="#" class="button success postfix">COUNT ME IN!</a>
							    </div>
							  </div>
							</form>
							<span class="spam">I do not SPAM or sell information. Ever!</span>
						</div>
				</div>

			
			</div>

				
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php comments_template(); ?>
		</article>
	<?php endwhile; // End the loop ?>

<?php get_footer(); ?>
