<?php
/*
Template Name: Home Page
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>

<div class="home_page section_container_1">
	<div class="row">
		<div class="small-5 medium-4 large-5 columns">
			<img class="hide-for-small-only" src="/wp-content/uploads/includes/marketing_mindset.png">
		</div>
		<div class="small-12 medium-8 large-7 columns">
			<div class="small-12 columns">
					<h1 class="main_title">Market Your Products &amp; Services, Genuinely.</h1>
					<p class="main_sub" style="font-size: 1.12rem;">Essential Marketing Knowledge For Freelancers &amp; Entrepreneurs</p>
			</div>
			<div class="small-12 medium-12 medium-centered large-12 large-centered columns">
					<a href="/about-you" class="button success expand">Let's Talk About You</a> 

			</div>
		</div>

	</div>	
</div>


<div class="home_page section_container_4">
	<div class="row">
		<div class="small-12 medium-12 medium-centered columns" role="main">

			<h3>Latest Thoughts</h3>

			<div class="row">

			<?php 
				$recent_posts = wp_get_recent_posts(array('numberposts' => 5, 'post_status' => 'publish',));
				foreach( $recent_posts as $recent ){
				echo '<a href="' . get_permalink($recent["ID"]) . '">';
					echo '<div class="small-12 columns">';
						echo '<div class="blog_post">';
							echo '<h4>' . $recent["post_title"] .'</h4>';
						echo '</div>';
					echo '</div>';
				echo '</a>';
				
				}
			?>
			</div>
		</div>
		<div class="small-12 medium-12 medium-centered columns">
			<a href="/thoughts" class="button success postfix">View All Posts</a>
		</div>
	</div>
</div>

<!-- <div class="home_page section_container_2">
	<div class="row">
		<div class="small-12">
			<div class="small-12 medium-12 medium-centered text-center columns">
				<p>“The concept of genuine marketing is based on trust. It’s a practice of building lasting relationships with the people who can genuinely benefit from your products or services.”</p>
				<a href="/thoughts" class="button expand">Genuine Marketing 101</a>
			</div>
		</div>

	</div>	
</div> 
<div class="home_page section_container_3">
	<div class="row">
		<div class="small-12 medium-3 hide-for-small-only columns">
				<img src="/wp-content/uploads/includes/inbox.png">
		</div>
		<div class="small-12 medium-9 columns">
		
			<h2>Subscribe to ATB! </h2>
			<p>Here are a few reasons why you should subscribe to my mailing list: </p>

			<ul>
				<li>&#8226; You’ll enroll in my 14 day course on Genuine Marketing (this is the only way to get it!) </li>
				<li>&#8226; You’ll receive valuable marketing information only available to my subscribers</li>
				<li>&#8226; You’ll never miss a post</li>
				<li>&#8226; You'll feel more awesome</li>  
			</ul>

		</div>
	</div>
	<div class="row">
			<div class="small-12 medium-12 medium-centered columns">
				<?php // echo do_shortcode('[yikes-mailchimp form="2"]'); ?>
				<span class="spam">I do not SPAM or sell information. Ever!</span>
			</div>
	</div>	
</div>
-->

<?php get_footer(); ?>
