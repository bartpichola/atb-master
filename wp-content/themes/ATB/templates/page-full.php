<?php
/*
Template Name: Full Width
*/
get_header(); ?>

<!-- <?php get_template_part( 'parts/featured-image' ); ?> -->

<div class="row">
	<div class="small-12 large-12 columns" role="main">

	<?php /* Start loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				<div class="about section_container_1">
					<div class="row">
						<div class="small-12 columns">
							<div class="small-12 medium-9 large-9 medium-centered large-centered columns">
								<h1 class="entry-title"><?php the_title(); ?></h1>
								<?php the_content(); ?>
							</div>
						</div>
					</div>	
				</div>
			</div>
			
		</article>
	<?php endwhile; // End the loop ?>

	</div>
</div>

<?php get_footer(); ?>
