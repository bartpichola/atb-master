<?php
/*
Template Name: ATB About Page Template
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>



	<?php /* Start loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				
			<div class="about section_container_1">
				<div class="row">
					<div class="small-12 columns">
						<div class="small-12 medium-9 large-9 medium-centered large-centered columns">
                            <h1>About You, The Entrepreneur.</h1>

								<p><strong>Today is the absolute best time in history to be an entrepreneur, because there has never been a better time for you to create, develop, or invent. </strong></p>
								<p>Two decades ago our common wisdom predicted a future that will be so complicated and so noisy that no single person could have accomplished anything of any significance. We’ve predicted the future in which we would rely on ever-larger corporations to innovate, because they had the resources and manpower needs to cut through the noise and complexity. We’ve predicted that the age of an entrepreneur was over. Oh boy were we wrong.</p>
								<p>Ironically, as the world became more complicated, entrepreneurship actually became easier. Access to cheaper and better computing power, better technology, more advanced robotics, artificial intelligence, and access to human brain power in form of crowdsourcing let’s you develop, invent, and innovate faster, better, and easier than ever before. </p>
								<p><strong>Likewise, you can now bring your product, service, or idea to market faster and cheaper than ever before.</strong> The same disruption that gave you the opportunity to create, also gave you the opportunity to market. </p>
								<p>Today, you can create and distribute your message to the right people almost instantly, needing nothing more than what's already in your pocket. And If your message is good enough, you can distribute your message to millions of people without spending a dime! Today you have more marketing power at your disposal than what the largest and most valuable brands had at their disposal just two decades ago!</p>
								<p>However, having an access to all these wonderful tools does not automatically grant you the ability to use them properly: that requires marketing knowledge. Unfortunately, this knowledge is not inherent, it’s acquired.</p>
								<p>True, you have access to more knowledge than ever before, but just as you’ve suspected, most of marketing “knowledge” tossed around is garbage. It’s theories built on bad assumptions, or marketing relics from dead corporations. Both of which are unfit in the modern digital age.</p>
								<p>These bad assumptions are responsible for much of the noise created today. The Internet is littered with poorly crafted messages, distributed to the wrong people. It's bad pollution that serves no one, not the marketer nor the public. </p>
								<p>The essential marketing knowledge is cocooned by inessential complexity. Some try to unpeel the complexity with varying degree of success, but instead of this, I propose something different: </p>
								<p>Let’s start from scratch; let’s dismantle marketing knowledge until nothing remains; let’s deconstruct it and then reconstruct it piece by piece, starting with the most essential components, free of complexity and the garbage it has accumulated. Let’s construct marketing knowledge that is effective, efficient, and essential: marketing knowledge that is fit for the modern age. </p>

						</div>
					</div>
				</div>	
			</div>


			<div class="one01 section_container_3">
			
				<div class="row">
					<div class="large-2 columns text-center">
						<i class="fa fa-envelope-o"></i>
					</div>
					<div class="large-10 columns">
					   <p>Subscribe to marketing tidbit of the week and receive "10 Things I Wish I Knew 10 Years Ago" success guide as a gift.</p>
					</div>
				</div>
				<div class="row">
						<div class="small-12 columns">
							<form>
							  <div class="row collapse">
							    <div class="small-2 large-1 columns">
							      <span class="prefix">Name</span>
							    </div>
							    <div class="small-1 large-4 columns">
							      <input type="text" placeholder="ex. John">
							    </div>
							    <div class="small-2 large-1 columns">
							      <span class="prefix">Email</span>
							    </div>
							    <div class="small-1 large-4 columns">
							      <input type="text" placeholder="John@awesome.com">
							    </div>
							    <div class="small-2 columns">
							          <a href="#" class="button success postfix">COUNT ME IN!</a>
							    </div>
							  </div>
							</form>
							<span class="spam">I do not SPAM or sell information. Ever!</span>
						</div>
				</div>

			
			</div>

				
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php comments_template(); ?>
		</article>
	<?php endwhile; // End the loop ?>

<?php get_footer(); ?>
