<?php
/*
Template Name: ATB Books Page Template
*/
get_header(); ?>

<?php
	function book($img_title, $book_title, $author, $desc, $am_url, $au_url) {

						echo '<div class="panel callout columns">';
						echo '	<div class="large-2 medium-4 columns">';
						echo '		<a href="'.$am_url.'"><img src="/wp-content/uploads/includes/books/'.$img_title.'.jpg"></a>';
						echo '	</div>';
						echo '	<div class="large-10 medium-8 columns">';
						echo '		<h3>'.$book_title.'</h3>';
						echo '		<h4><i>by '.$author.'</i></h4>';
						echo '		<p>'.$desc.'</p>';
						echo '		<hr>';
						echo '		<div class="large-4 medium-8 columns">';
						echo '			<a href="'.$am_url.'" target="_blank" class="button alert expand">Amazon</a>';
						echo '		</div>';
						echo '		<div class="large-4 medium-8 columns end">';
						echo '			<a href="'.$au_url.'" target="_blank" class="button alert expand">Audible (audio book)</a>';
						echo '		</div>';
						echo '	</div>';
						echo '</div>';

	}
?>

<?php //get_template_part( 'parts/featured-image' ); ?>

	<?php /* Start loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				
			<div class="books section_container_1">
				<div class="row">
					<div class="small-12 columns">
						<h1>Bart's Favorite Books</h1>
					</div>

					<div class="small-12 medium-8 columns">
						<p>Don’t try to figure it out yourself, you don’t have enough lifetime to do so. Instead, read books and acquire the knowledge from others. Books are neat little packages of condensed information which took someone decades--if not a lifetime--to learn. Take advantage of that!</p>
						<p>Reading is the single most effective activity you can do with your time. Hours of reading are worth years of trying. </p>
						<p>In this section I list my favorite books. These books have inspired me, and represent bulk of the knowledge which I share with you through ATB. So I highly recommend that you pick some of them up.</p>

					</div>
					<div class="small-12 medium-4 columns">
						<img src="/wp-content/uploads/includes/marketing_mindset.png">
					</div>
					<div class="small-12 columns">
						<div class="panel callout">
						  <h5>Please Note:</h5>
						  <p>I’ve included links to <a href="http://amzn.to/29ZudIN" target="_blank">amazon</a> and <a href="http://amzn.to/2aFxZJ7" target="_blank">audible</a> for each book. If you choose to purchase the book using the following links, I get a small commission from the sales: it costs you nothing, but it helps me a lot. If you do choose to use the link, then please know that you’re super awesome!</p>
						</div>			
					</div>		

				</div>	
				<div class="row">
				
					<div class="small-12 columns">
						<h2>Top 10 Must Read Books</h2>
						<p>Here are books which contributed to shaping my mindset the most. I revisit these books often.  </p>
					</div>

					<div class="large-12 columns">

						<?php 

							// book(
							// 	//Img title
							// 	"",
							// 	//Book title
							// 	"",
							// 	//Author
							// 	"",
							// 	//Description
							// 	"",
							// 	//Amazon Link
							// 	"",
							// 	//Audible Link
							// 	""
							// );

							book(
								//Img title
								"how_to_win_friends_and_influance_people_book_cover",
								//Book title
								"How To Win Friends &amp; Influance People",
								//Author
								"Dale Carnegie",
								//Description
								"This is one of my favorite books of all time, and I feel it should be a mandatory textbook on the subject of human relations. This book will help you more with marketing, and dealing with people in general, than any other book out there. I highly recommend this to be your first read. I also highly recommend the audiobook narrated by Andrew MacMillan. His voice is magical. ",
								//Amazon Link
								"http://amzn.to/2a7RKXJ",
								//Audible Link
								"http://amzn.to/2adST4E"
							);

							book(
								//Img title
								"essencialism_book_cover",
								//Book title
								"Essencialism",
								//Author
								"Greg McKeown",
								//Description
								'"You cannot overestimate the unimportance of practically everything.” John Maxwell. Essentialism is about cutting out the unimportant and focusing on what is important instead. I’ve read this book many times and it helped me in all areas of life. Not just marketing. This books is pure gold for developing your mindset.  ',
								//Amazon Link
								"http://amzn.to/2f5fUoe",
								//Audible Link
								"http://amzn.to/2f5fUoe"
							);

							book(
								//Img title
								"so_good",
								//Book title
								"So Good They Can't Ignore You",
								//Author
								"Cal Newport",
								//Description
								"A book that every high school student should read, and any marketer who is trying to “find him/herself”. The message of the book can be quite challenging to some. Especially the firm believer of the “follow your passion” theory. The gist of the book is basically this: don’t follow your passion, develop it instead.",
								//Amazon Link
								"http://amzn.to/2flMVNR",
								//Audible Link
								"http://amzn.to/2flMVNR"
							);

							book(
								//Img title
								"how_to_fail",
								//Book title
								"How to Fail at Almost Everything and Still Win Big",
								//Author
								"Scott Adams",
								//Description
								"This book is as entertaining as educational. Written by Scott Adams, the creator of the Dilbert comic strip. So if you enjoy Dilbert, then you’ll enjoy this book too. It’s a book of how one failure after another can lead to success. I like this book because it has lots of good advice, and it’s comforting in an odd way.  ",
								//Amazon Link
								"http://amzn.to/2g0AJUc",
								//Audible Link
								"http://amzn.to/2f5f9vv"
							);

							book(
								//Img title
								"think_and_grow_rich",
								//Book title
								"Think and Grow Rich",
								//Author
								"Napoleon Hill",
								//Description
								"I love this book. I’ve read it about 10 times in the past decade. In it, Napoleon eludes to a “Secret” which will reveal itself only to the people who are ready for it. I have to admit that I was confused as to what the secret was for a while. But then I realized that the secret is so simple and so in your face, that no wonder that so many people overlook it. But, the secret is probably the most important life lesson I’ve learned. ",
								//Amazon Link
								"http://amzn.to/2flRWGg",
								//Audible Link
								"http://amzn.to/2flRWGg"
							);

							book(
								//Img title
								"stop_worrying",
								//Book title
								"How to Stop Worrying and Start Living",
								//Author
								"Dale Carnegie",
								//Description
								"Another great book from Dale Carnegie. I reach for that book whenever I feel stressed or overwhelmed. Two chapters in, I’m fine again. The title of the book is very descriptive, so I’m not sure what else to say about it. Again, I recommend the audiobook version narrated by Andrew MacMillan.",
								//Amazon Link
								"http://amzn.to/2f6ch1i",
								//Audible Link
								"http://amzn.to/2fa2ISw"
							);

							book(
								//Img title
								"steal_like_an_artist",
								//Book title
								"Steal Like an Artist",
								//Author
								"Austin Kleon",
								//Description
								"Austin Kleon is the man. I feel that this book gave me permission to steal other people’s ideas and build upon them, without feeling like a hack. I know that this sentence is hard to comprehend now, but after you read this short book, you’ll understand exactly what I’m talking about.",
								//Amazon Link
								"http://amzn.to/2gorfpC",
								//Audible Link
								"http://amzn.to/2gorfpC"
							);

							book(
								//Img title
								"show_your_work",
								//Book title
								"Show Your Work!",
								//Author
								"Austin Kleon",
								//Description
								"Another gold from Austin Kleon. This book put me over the edge and is one of the main contributors to why you’re reading ATB. It’s filled with great content that explains exactly what you should be doing right now, while inspiring to actually do it.  ",
								//Amazon Link
								"http://amzn.to/2f6eJoE",
								//Audible Link
								"http://amzn.to/2f6eJoE"
							);

			

							book(
								//Img title
								"influence_book_cover",
								//Book title
								"Influence: The Psychology of Persuasion",
								//Author
								"Robert B. Cialdini",
								//Description
								"I have to admit that I was conflicted about this book for a long time. In general, I don't like things which can be misconstrued to take advantage of others. The study of persuasion can be used to manipulation. And as I say often, there is no room for that in marketing. But, Influence, is an important book which offers a great insight into how easily people can be manipulated. What you do with the information is up to you. ",
								//Amazon Link
								"http://amzn.to/2gous8B",
								//Audible Link
								"http://amzn.to/2g1hb0w"
							);

							book(
								//Img title
								"power_of_habits",
								//Book title
								"The Power of Habit",
								//Author
								"Charles Duhigg",
								//Description
								"Habits are everything, because they represent majority of your day. They dictate how you do certain things, and when you do them. You live, for the most part, on auto-pilot. I’m sure you have lots of bad habits in your life which you’ll want to change. And I'm sure that you want to form good habits too. This book talks about the mechanics of habit formation, so you can change your old habits, and develop new ones too. ",
								//Amazon Link
								"http://amzn.to/2goyBsT",
								//Audible Link
								"http://amzn.to/2f6hE0J"
							);	


						 ?>

					</div>
					<div class="small-12 columns">
						<h2>Other Great Reads</h2>
						<p>Here’s a list of books which I love and can recommend to you. They are listed in no particular order.</p>
					</div>
					<div class="large-12 columns">

						<?php 

							book(
								//Img title
								"antifragile",
								//Book title
								"Antifragile",
								//Author
								"Nassim Nicholas Taleb",
								//Description
								"Big book. Scary at times too. But Nassim Taleb tells you like it is. After reading this book, you will not look at any organization, or the world of predictions in the same way again. You will realize just how fragile the structure of our economy, government, and even most corporations is. This book will change your worldview, that's for sure. ",
								//Amazon Link
								"http://amzn.to/2g1AQ1N",
								//Audible Link
								"http://amzn.to/2f6dNjZ"
							);


							book(
								//Img title
								"tipping_point",
								//Book title
								"The Tipping Point",
								//Author
								"Malcolm Gladwell",
								//Description
								"Malcolm Gladwell is a great story teller. I enjoy pretty much everything I pick up that was written by him. And this book of his is probably my favorite one. The book is about how ideas, trends, or social behaviours spread. Or how, once they reach a critical tipping point, the ideas go “viral”. ",
								//Amazon Link
								"http://amzn.to/2f6dfdU",
								//Audible Link
								"http://amzn.to/2goxtWm"
							);

						    book(
								//Img title
								"mistakes_were_made",
								//Book title
								"Mistakes Were Made (But Not By Me)",
								//Author
								"Carol Tavris and Elliot Aronson",
								//Description
								"A great book, and a very important book, on the topic of cognitive biases. We all have them and we’re all affected by them. Some examples used in the book are scary. But, if you’re going to grow your own mindset, you need to be aware of your own biases. ",
								//Amazon Link
								"http://amzn.to/2g2nncr",
								//Audible Link
								"http://amzn.to/2goDvq0"
							);
							
							book(
								//Img title
								"stumbling_on_happiness",
								//Book title
								"Stumbling on Happiness",
								//Author
								"Daniel Gilbert",
								//Description
								"The book talks about happiness, but not from the self-help point of view. No, this book will not make you happier. Instead, this book covers the concept of happiness from the psychological point of view. It has many great examples, it’s information dense, but delivers it in an entertaining way. Every other sentence will make you say “huh!”. ",
								//Amazon Link
								"http://amzn.to/2goyA8B",
								//Audible Link
								"http://amzn.to/2f6dMwq"
							);
							
							book(
								//Img title
								"war_of_art",
								//Book title
								"The War of Art",
								//Author
								"Steven Pressfield",
								//Description
								"Great, great, great book. Almost made it into my top 10 list. It’s a book about overcoming that little voice in the back of your head that tells you to just give up and watch TV instead. It’s a book about moving away from being an amateur and going pro. ",
								//Amazon Link
								"http://amzn.to/2g2o996",
								//Audible Link
								"http://amzn.to/2g1KCko"
							);
							
							book(
								//Img title
								"mindwise",
								//Book title
								"Mindwise",
								//Author
								"Nicholas Epley",
								//Description
								"Mindwise is a book about tapping into your “6th sense”. That gut feeling you have about something before you have time to formulate a thought about it. It’s also about “reading minds”. Strangely, it works. ",
								//Amazon Link
								"http://amzn.to/2goDylH",
								//Audible Link
								"http://amzn.to/2fa6CdO"
							);
							
							book(
								//Img title
								"switch",
								//Book title
								"Switch",
								//Author
								"Chip Heath",
								//Description
								"This book cover the topic of making lasting changes within organizations, and your own life. Why is it so hard to create a lasting change? This book goes into the psychology of change. ",
								//Amazon Link
								"http://amzn.to/2g2vdTv",
								//Audible Link
								"http://amzn.to/2goFTgs"
							);
							
							book(
								//Img title
								"berwing_up_business",
								//Book title
								"Brewing Up a Business",
								//Author
								"Sam Calagione",
								//Description
								"I love good beer, I love brewing good beer, I love Dogfish Head, I love Sam Calagione, and I love a good success story. This book has it all! It’s a book that talks about the struggles, fights, and adventures that Sam faced in order to get his brewery to where it is today (or at least where it was when he wrote the book). ",
								//Amazon Link
								"http://amzn.to/2fab7oO",
								//Audible Link
								"http://amzn.to/2facl3w"
							);
							
							book(
								//Img title
								"delivering_happiness",
								//Book title
								"Delivering Happiness",
								//Author
								"Tony Hsieh",
								//Description
								"Another great success story--this one is of the online shoe retailer zappos. Tony Hsieh tells a story of the struggles, and how he had to change directions so many times. His adventure sounds like a good hollywood story where everything is always at the brink of collapse. Which reminds me of my early career. Good read. ",
								//Amazon Link
								"http://amzn.to/2f6n1gq",
								//Audible Link
								"http://amzn.to/2g1Kt0j"
							);
							
							book(
								//Img title
								"losing_my_virginity",
								//Book title
								"Losing My Virginity",
								//Author
								"Richard Branson",
								//Description
								"If you like Richard Branson, then you’ll love this book. Enough said. ",
								//Amazon Link
								"http://amzn.to/2g1LC7X",
								//Audible Link
								"http://amzn.to/2g1LC7X"
							);
							
							book(
								//Img title
								"everything_store",
								//Book title
								"The Everything Store",
								//Author
								"Brad Stone",
								//Description
								"The story of Amazon. Great inspirational read for all of us “wannabe” entrepreneurs. Basically covers how Jeff Bezos went from his garage, and a little bell that would notify him when a book got ordered, to the giant behemoth that Amazon is today. ",
								//Amazon Link
								"http://amzn.to/2g1ieNS",
								//Audible Link
								"http://amzn.to/2f6ovXW"
							);
							
							book(
								//Img title
								"checklist_manifesto",
								//Book title
								"The Checklist Manifesto",
								//Author
								"Atul Gawande",
								//Description
								"You would think that a subject of checklist would be as dull as reading off of one. But this book is fascinating. So many good stories, especially in the medical field. The social order of hospitals was (and maybe still is) just crazy! But, the most importantly lesson I took was how something extremely simple, and so often overlooked, can have such an enormous impact. I have borrowed a lot of concepts from this book and adopted them into marketing. ",
								//Amazon Link
								"http://amzn.to/2fmQQtY",
								//Audible Link
								"http://amzn.to/2f6eHgs"
							);
							
							book(
								//Img title
								"getting_things_done",
								//Book title
								"Getting Things Done",
								//Author
								"David Allen",
								//Description
								"This one is a classic organization book. I do have to admit that it’s a bit of dull read, but the instructions are clear, and I have successfuly adapted the system (at least in my own form) in my daily life. If you’re drowning due to disorganization, than this book is for you. ",
								//Amazon Link
								"http://amzn.to/2g1kV2j",
								//Audible Link
								"http://amzn.to/2f6nOOp"
							);
							
							book(
								//Img title
								"7_habits",
								//Book title
								"The 7 Habits of Highly Effective People",
								//Author
								"Stephen R. Covey",
								//Description
								"Another classic. I’m pretty sure that every worthy business person has read this book at some point. The book covers topics of fairness, integrity, dignity, and more. All in all, an inspirational book. ",
								//Amazon Link
								"http://amzn.to/2fac4gN",
								//Audible Link
								"http://amzn.to/2fL3R13"
							);
							
							book(
								//Img title
								"one_sentence",
								//Book title
								"The One Sentence Persuasion Course",
								//Author
								"Blair Warren",
								//Description
								"This book is extremely short, but the information is gold. Again, I was conflicted by the whole concept of persuasion, but I understand the value of it now. This book is more of a course than a book, and I’m not going to say much about it right now. Just read it. ",
								//Amazon Link
								"http://amzn.to/2fmWDQn",
								//Audible Link
								"http://amzn.to/2fmWDQn"
							);
							
							book(
								//Img title
								"thinking_fast",
								//Book title
								"Thinking, Fast and Slow",
								//Author
								"Daniel Kahneman",
								//Description
								"Basically, we have two mind: a highly analytical mind, which is slow, and a highly reactive one, which is fast. This book talks about the two minds, and how to harness their powers. I highly recommend it. ",
								//Amazon Link
								"http://amzn.to/2fKYQp4",
								//Audible Link
								"http://amzn.to/2fKYQp4"
							);

							book(
								//Img title
								"22_laws",
								//Book title
								"The 22 Immutable Laws of Marketing",
								//Author
								"Al Ries and Jack Trout",
								//Description
								"Although some “laws” I feel are dated (the book was written in 1994), this book does hold a lot of great information about marketing. It’s geared more toward corporate level of marketing, but you can derive value from the information at almost every level. ",
								//Amazon Link
								"http://amzn.to/2fabSOF",
								//Audible Link
								"http://amzn.to/2gmh2cv"
							);

							book(
								//Img title
								"on_writing_well",
								//Book title
								"On Writing Well",
								//Author
								"William Zinsser",
								//Description
								"Great, great, great book about writing. It’s my go-to book on the subject and I reference it a lot. The book is not about grammar, or sentence structure, but rather about the craft of writing. I, by no means, can claim to be a good writer, but this book helped me get better. Also, as you can imagine, the book is well written. ",
								//Amazon Link
								"http://amzn.to/2g1SFxE",
								//Audible Link
								"http://amzn.to/2g1SFxE"
							);

							book(
								//Img title
								"clean_sentences",
								//Book title
								"Clean, Well-Lighted Sentences",
								//Author
								"Janis Bell",
								//Description
								"This is the book about grammar and sentence structure, which I should reference more often (for obvious reasons). ",
								//Amazon Link
								"http://amzn.to/2gmfsHs",
								//Audible Link
								"http://amzn.to/2gmfsHs"
							);

							book(
								//Img title
								"abandance",
								//Book title
								"Abundance",
								//Author
								"Peter H. Diamandis and Steven Kotler",
								//Description
								"I’m a big fan of Peter Diamandis. He is a super human who just cannot stop inventing or investing in ridiculous projects. Like, space tourism, or mining asteroids for precious minerals. What I like about this book is that it goes against the pop-culture dystopian/apocalyptic view of the future, and reassures you that everything will be all right. This book makes me happy.",
								//Amazon Link
								"http://amzn.to/2g1lc5c",
								//Audible Link
								"http://amzn.to/2fmTsbg"
							);																					

							book(
								//Img title
								"bold",
								//Book title
								"Bold",
								//Author
								"Peter H. Diamandis and Steven Kotler",
								//Description
								"Another great book from Peter Diamandis and Steven Kotler. This one is related to Abundance, but it’s more of an instruction manual of how to accomplish the things which Abundance talks about. In other words, how to think big. If you’re an entrepreneur and needs an inspiration, then this book is for you.",
								//Amazon Link
								"http://amzn.to/2g1V7UE",
								//Audible Link
								"http://amzn.to/2gmidZf"
							);		

							book(
								//Img title
								"4_hour_workweek",
								//Book title
								"The 4-Hour Workweek",
								//Author
								"Timothy Ferriss",
								//Description
								"Tim Ferriss, he’s everywhere! How does he do that? I do love his podcast and I do like this book too. The book is not about working 4-hours a week, but rather how to do more with less time. Or how to 10x your output. Great content and the first few chapters are inspiring.",
								//Amazon Link
								"http://amzn.to/2g1ZXBg",
								//Audible Link
								"http://amzn.to/2gmcimW"
							);		

							book(
								//Img title
								"surely_youre_joking",
								//Book title
								"Surely You're Joking, Mr. Feynman!",
								//Author
								"Richard P. Feynman",
								//Description
								"Richard Feynman, what a man, what a scientist, what a joker, prankster, and a hack! I love him. His stories are great! If my life is anywhere as colorful as his, then I’ll die happy. ",
								//Amazon Link
								"http://amzn.to/2fmZlp1",
								//Audible Link
								"http://amzn.to/2gmhaZb"
							);		

							book(
								//Img title
								"the_inevitable",
								//Book title
								"The Inevitable",
								//Author
								"Kevin Kelly",
								//Description
								"Kevin Kelly! The co-founder of wired magazine. Immensely interesting person. This book investigates current trends, and based on that it tries to “predict” (although that’s a lose interpretation) how the next 50 years or so will look like, as far as technology goes. The book is good, because it’s trying to predict the future based on human nature, interests, etc… Which in my opinion, is not likely to change. ",
								//Amazon Link
								"http://amzn.to/2gmdKFQ",
								//Audible Link
								"http://amzn.to/2g1XRBI"
							);		

							book(
								//Img title
								"black_swan",
								//Book title
								"The Black Swan",
								//Author
								"Nassim Nicholas Taleb",
								//Description
								"Another great read from Nassim Taleb. This one predates Antifragile, and covers a lot of the same topics that Antifragile covers. However, the main topic is the unpredictability of random events. That no matter how much we want to predict the future, we will never be able to predict it, because the future will be hugely affected by random events which we have no way of predicting. ",
								//Amazon Link
								"http://amzn.to/2f6rCzb",
								//Audible Link
								"http://amzn.to/2g1XF5l"
							);																							


							book(
								//Img title
								"predictably_irrational",
								//Book title
								"Predictably Irrational",
								//Author
								"Dan Ariely",
								//Description
								"Another great book on the topic of human nature, psychology, and most importantly, why people are acting so irrationally. As it turns out, there are mechanics behind our irrationality, to the point that irrationality is predictable. ",
								//Amazon Link
								"http://amzn.to/2fL4Luv",
								//Audible Link
								"http://amzn.to/2g1l2un"
							);		


							book(
								//Img title
								"outliers",
								//Book title
								"Outliers",
								//Author
								"Malcolm Gladwell",
								//Description
								"Another great Malcolm Gladwell book. This one talks about the people who seem to reach levels beyond possible. It talks about successful businessmen, athletes, etc. The book is full of fascinating stories, told in a captivating way.",
								//Amazon Link
								"http://amzn.to/2fai7C2",
								//Audible Link
								"http://amzn.to/2fakSnb"
							);		


							book(
								//Img title
								"sapiens",
								//Book title
								"Sapiens",
								//Author
								"Yuval Noah Harari",
								//Description
								"What a book! It’s a 20,000 foot overview of the human race, and incredibly fascinating! I had to read it twice! It covers sapiens from ape like creatures a million years ago to now (and beyond). It talks about all the revolutions: cognitive revolution, agricultural revolution, industrial revolution, you name it. Must read for any human.",
								//Amazon Link
								"http://amzn.to/2faiQDC",
								//Audible Link
								"http://amzn.to/2g24WBY"
							);		


							book(
								//Img title
								"tribes",
								//Book title
								"Tribes",
								//Author
								"Seth Godin",
								//Description
								"Seth Godin is my favorite marketing guy. If you know of him, then you can probably recognize his influence in my writing. Tribes is a great book about tapping into a small group of like minded people who communicate with each other, and who are willing to spread your message of value. ",
								//Amazon Link
								"http://amzn.to/2fL5vjh",
								//Audible Link
								"http://amzn.to/2fagi8g"
							);	

							book(
								//Img title
								"permission_marketing",
								//Book title
								"Permission Marketing",
								//Author
								"Seth Godin",
								//Description
								"Hugely inspirational book, and in part, a blueprint for Genuine Marketing. If you want to understand the principles behind the Genuine Marketing, then you need to read this book. Just a forewarning, the book was written in 1998. All references to the internet and technology are extremely outdated. But the general concept is true to this day. ",
								//Amazon Link
								"http://amzn.to/2g20rHR",
								//Audible Link
								"http://amzn.to/2g26X10"
							);	

							book(
								//Img title
								"marketers_are_liars",
								//Book title
								"All Marketers Are Liars",
								//Author
								"Seth Godin",
								//Description
								"This is one of Seth Godin’s failed books. He even admits it. But the information is still great. It’s a book about the stories that marketers tell, the stories we tell ourselves, our worldview, people’s worldviews, and other great topics. I highly recommend it. ",
								//Amazon Link
								"http://amzn.to/2f6iuKS",
								//Audible Link
								"http://amzn.to/2f6tQhN"
							);	

						 ?>
					</div>
					<div class="small-12 columns">
						<p>All of the books which are on this page are worthwhile reads. I’ve read a lot more books too, but a lot of them did not make the cut. I’ll be continuously adding to this list as I find new worthwhile books to read. If you have a suggestion for a book that I should read, please email me! </p>
					</div>
				</div>
			</div>
			</div>
			<?php comments_template(); ?>
		</article>
	<?php endwhile; // End the loop ?>

<?php get_footer(); ?>
