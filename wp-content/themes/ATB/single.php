<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>



	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	<div class="post_title_container">
		<div class="row">
			<div class="small-12 medium-10 large-8 medium-centered large-centered columns">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>	
	</div>
	<div class="row">	
		<div class="small-12 medium-10 large-8 medium-centered large-centered columns" role="main">
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
				<div class="post_content_container entry-content">

					<?php if ( has_post_thumbnail() ) : ?>
						<div class="row">
							<div class="column">
								<!-- <?php the_post_thumbnail( '', array('class' => 'th') ); ?> -->
							</div>
						</div>
					<?php endif; ?>		
					<?php the_content(); ?>

					<hr>
					<h5>Did you enjoy this article?</h5>
					<p>If so, please share it with the people who you think can benefit from it using the links below.</p>
					<?php echo do_shortcode( '[addtoany]' ); ?>
					<!-- <hr>
					<h5>Would you like to discuss?</h5>
					<p>If you would like to discuss this thought, you can always reach out to me on <a href="http://twitter.com/atb_bart"><i class="fa fa-twitter" aria-hidden="true"></i> twitter</a>, <a href="http://facebook.com/accordingtobart"><i class="fa fa-facebook" aria-hidden="true"></i> facebook</a>, or email me directly. -->
					<hr>
					<h5>Would you like to read more?</h5>
					<a class="button expand" href="/thoughts">Read more thoughts</a>
				</div>
				<footer>
					<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
					<p><?php the_tags(); ?></p>
				</footer>
				<?php do_action( 'foundationpress_post_before_comments' ); ?>
				<?php comments_template(); ?>
				<?php do_action( 'foundationpress_post_after_comments' ); ?>
			</article>
		<?php endwhile;?>

		<?php do_action( 'foundationpress_after_content' ); ?>

		</div>
	</div>
<?php get_footer(); ?>