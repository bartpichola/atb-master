<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
<style>
	.thoughts li { list-style: none; line-height: 1.5rem; margin-bottom: 2.5rem; }
</style>
<div class="row">
	<?php //get_template_part( 'parts/check-if-sidebar-exist' ); ?>
	<div class="small-12 medium-12 large-12 columns margin-top thoughts">
		<h1>Thoughts on Marketing</h1>
		<p>Here are thoughts and articles on marketing which I hope you'll find valuable.</p>
		<hr>
		<ul>
		<?php if ( have_posts() ) : ?>
			<?php do_action( 'foundationpress_before_content' ); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a><span style="font-size:0.7rem; color: #555; margin-left:0.5rem;">Posted: <?php echo get_the_date(); ?></span></li> 
			<?php endwhile; ?>
			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			<?php do_action( 'foundationpress_before_pagination' ); ?>
		<?php endif;?>
		</ul>

	</div>


	<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php } ?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
	<?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>
